#!/bin/bash
set -e

docker pull node:10-alpine
docker pull node:13-alpine
docker pull node:14-alpine
docker pull node:16-alpine
docker pull node:19-alpine
docker pull node:20-alpine
docker pull node:21-alpine

docker build -f legacy/10/Dockerfile --build-arg NODE_VERSION=10 -t test-node-10 .
docker build -f legacy/Dockerfile --build-arg NODE_VERSION=13 -t test-node-13 .
docker build -f legacy/Dockerfile --build-arg NODE_VERSION=14 -t test-node-14 .
docker build -f legacy/Dockerfile --build-arg NODE_VERSION=16 -t test-node-16 .
docker build -f legacy/Dockerfile --build-arg NODE_VERSION=19 -t test-node-19 .

docker build -f current/Dockerfile --build-arg NODE_VERSION=20 -t test-node-20 .
docker build -f current/Dockerfile --build-arg NODE_VERSION=21 -t test-node-21 .

docker image ls | grep "test-node-"