# Docker Node Images

## 1.0.1 - 02/10/2024
- feat(legacy/10): add missing libpng and libpng-dev packages

## 1.0.0 - 16/07/2024
- feat(archi): create legacy (<= 19) and current (> 19) dockerfiles
- feat(qa): add script to test builds
- feat(ci): add images node 20 and 21
- feat(ci): add tag to image for build